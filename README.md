# BikeComputer

# [DISCONTINUED]

A FOSS BikeComputer

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/de.nulide.bikecomputer/)

## Features

- Meassures your speed.
- Keep track of your stats. (Distance, Maximum Speed, TravelTime, ...)

## Contributers

Zine Eddine Hadj Rabah [@zhrdev354](https://gitlab.com/zhrdev354)

## Open Source Library/Assets

[See Licenses](ThirdPartyProjects.md)

## Donate

<script src="https://liberapay.com/Nulide/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/Nulide/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
