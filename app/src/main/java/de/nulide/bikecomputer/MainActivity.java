package de.nulide.bikecomputer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.nulide.bikecomputer.io.IO;
import de.nulide.bikecomputer.obj.Data;
import de.nulide.bikecomputer.obj.Settings;
import de.nulide.bikecomputer.ui.MainPageAdapter;

public class MainActivity extends AppCompatActivity implements LocationListener, View.OnTouchListener, TextWatcher, AdapterView.OnItemSelectedListener, View.OnClickListener {

    private final int PERM_LOCATION_REQUEST_CODE = 7161;
    private final int FILE_CHOOSER_SAVE_REQUEST_CODE = 8914;
    private final int FILE_CHOOSER_READ_REQUEST_CODE = 8916;
    private LinearLayout lMain;
    private TabLayout tabBar;
    private TextView tvSpeed;
    private TextView tvSpeedUnit;
    private TextClock tcClock;

    private TextView tvDistance;
    private TextView tvDistanceUnit;
    private TextView tvMaxSpeed;
    private TextView tvMaxSpeedUnit;
    private TextView tvAvgSpeed;
    private TextView tvAvgSpeedUnit;
    private TextView tvTravelTime;

    private TextView tvTripDistance;
    private TextView tvTripDistanceUnit;
    private TextView tvTripMaxSpeed;
    private TextView tvTripMaxSpeedUnit;
    private TextView tvTripAvgSpeed;
    private TextView tvTripAvgSpeedUnit;
    private TextView tvTripTravelTime;

    private Spinner spMetric;
    private EditText etMinimalViewSeconds;
    private EditText etRunInBackgroundForMinutes;
    private Button btnImportData;
    private Button btnExportData;
    private FloatingActionButton btnBell;
    private MediaPlayer mediaPlayer;

    private Timer timerLM;
    private LocationListener locationListener;
    private LocationManager locationManager;

    private int time = 1000; // = 1 sec.

    private Timer timerMV;
    private boolean isMinimalView = false;

    private Data data;
    private Data tripdata;
    private Settings settings;
    private int dataWriteInterval = 5;
    private int round = 0;
    private float speed;
    private float speedMS;

    private float toMPH = (float) 0.6213711922;
    private float toKMH = (float) 1.609344;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                    PERM_LOCATION_REQUEST_CODE);
        }

        btnBell  = findViewById(R.id.btnBell);
        btnBell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.bicycle_bell);
                mediaPlayer.start();
            }
        });

        data = IO.readData(getFilesDir());
        tripdata = new Data();
        settings = IO.readSettings(getFilesDir());

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, time, 1, this);
        locationListener = this;

        lMain = findViewById(R.id.lMain);
        tvSpeed = findViewById(R.id.tvSpeed);
        tvSpeedUnit = findViewById(R.id.tvSpeedUnit);
        tcClock = findViewById(R.id.tcClock);
        ViewPager pager = findViewById(R.id.pager);
        pager.setOnTouchListener(this);
        tabBar = findViewById(R.id.tabBar);
        tabBar.setupWithViewPager(pager);
        pager.setAdapter(new MainPageAdapter(this));
        reloadViews();
        updateViews();

        lMain.setOnTouchListener(this);
        timerMV = new Timer();
        timerMV.schedule(getNewTimerTask(), settings.getMinimalViewSeconds() * 1000);
    }


    @Override
    protected void onPause() {
        super.onPause();
        timerLM = new Timer();
        timerLM.schedule(new TimerTask() {
            @Override
            public void run() {
                locationManager.removeUpdates(locationListener);
            }
        }, settings.getBackgroundMinutes() * 60000);
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        if (timerLM != null) {
            timerLM.cancel();
            timerLM = null;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, time, 1, this);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERM_LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    finish();
                }
                return;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        float oldSpeedMS = speedMS;
        speedMS = location.getSpeed();
        if(speedMS > 0.025 || oldSpeedMS > 0.025) {
            if (settings.getMetric().equals(Settings.METRIC_MPH)) {
                speedMS *= toMPH;
            }
            speed = ((speedMS * 3600) / 1000);

            data.distance += speedMS;
            tripdata.distance += speedMS;
            data.travelTime += (time / 1000);
            tripdata.travelTime += (time / 1000);
            if (data.maxSpeed < speed) {
                data.maxSpeed = speed;
            }
            if (tripdata.maxSpeed < speed) {
                tripdata.maxSpeed = speed;
            }
            updateViews();
        }
        if(round < dataWriteInterval){
            round++;
        }else{
            IO.writeData(data, getFilesDir());
            round = 0;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        tvSpeed.setText("Signal Lost");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        onTouchSetTimers();
        hideKeyboard();
        return false;
    }

    private TimerTask getNewTimerTask(){
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(tabBar.getSelectedTabPosition() == 3){
                            timerMV.cancel();
                            timerMV = new Timer();
                            timerMV.schedule(getNewTimerTask(),settings.getMinimalViewSeconds()*1000);
                        }else {
                            lMain.setBackgroundColor(Color.BLACK);
                            tvSpeed.setTextColor(Color.WHITE);
                            tvSpeedUnit.setTextColor(Color.WHITE);
                            tcClock.setTextColor(Color.WHITE);
                            btnImportData.setVisibility(View.GONE);
                            btnExportData.setVisibility(View.GONE);
                            isMinimalView = true;
                            tabBar.setVisibility(View.GONE);
                        }
                    }
                });
            }
        };
        return timerTask;
    }
    
    public void reloadViews(){
        tvDistance = findViewById(R.id.tvDistance);
        tvDistanceUnit = findViewById(R.id.tvDistanceUnit);
        tvMaxSpeed = findViewById(R.id.tvMaxSpeed);
        tvMaxSpeedUnit = findViewById(R.id.tvMaxSpeedUnit);
        tvAvgSpeed = findViewById(R.id.tvAvgSpeed);
        tvAvgSpeedUnit = findViewById(R.id.tvAvgSpeedUnit);
        tvTravelTime = findViewById(R.id.tvTravelTime);
        tvTripDistance = findViewById(R.id.tvTripDistance);
        tvTripDistanceUnit = findViewById(R.id.tvTripDistanceUnit);
        tvTripMaxSpeed = findViewById(R.id.tvTripMaxSpeed);
        tvTripMaxSpeedUnit = findViewById(R.id.tvTripMaxSpeedUnit);
        tvTripAvgSpeed = findViewById(R.id.tvTripAvgSpeed);
        tvTripAvgSpeedUnit = findViewById(R.id.tvTripAvgSpeedUnit);
        tvTripTravelTime = findViewById(R.id.tvTripTravelTime);
        spMetric = findViewById(R.id.spMetric);
        spMetric.setOnItemSelectedListener(this);
        ArrayAdapter<String> adapter;
        List<String> spMetricList = new ArrayList<>();
        spMetricList.add(Settings.METRIC_KMH);
        spMetricList.add(Settings.METRIC_MPH);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, spMetricList);
        spMetric.setAdapter(adapter);
        switch(settings.getMetric()){
            case Settings.METRIC_KMH:
                spMetric.setSelection(0);
                break;
            case Settings.METRIC_MPH:
                spMetric.setSelection(1);
                break;
        }
        etMinimalViewSeconds = findViewById(R.id.etMinimalViewSeconds);
        etMinimalViewSeconds.setText(String.valueOf(settings.getMinimalViewSeconds()));
        etMinimalViewSeconds.addTextChangedListener(this);
        etRunInBackgroundForMinutes = findViewById(R.id.etRunInBackgroundForM);
        etRunInBackgroundForMinutes.setText(String.valueOf(settings.getBackgroundMinutes()));
        etRunInBackgroundForMinutes.addTextChangedListener(this);
        btnImportData = findViewById(R.id.btnImportData);
        btnImportData.setOnClickListener(this);
        btnExportData = findViewById(R.id.btnExportData);
        btnExportData.setOnClickListener(this);
        reloadText();
    }

    public void reloadText(){
        switch(settings.getMetric()){
            case Settings.METRIC_KMH:
                tvSpeedUnit.setText(Settings.METRIC_KMH);
                tvDistanceUnit.setText(Settings.METRIC_KM);
                tvMaxSpeedUnit.setText(Settings.METRIC_KMH);
                tvAvgSpeedUnit.setText(Settings.METRIC_KMH);
                tvTripDistanceUnit.setText(Settings.METRIC_KM);
                tvTripMaxSpeedUnit.setText(Settings.METRIC_KMH);
                tvTripAvgSpeedUnit.setText(Settings.METRIC_KMH);
                break;
            case Settings.METRIC_MPH:
                tvSpeedUnit.setText(Settings.METRIC_MPH);
                tvDistanceUnit.setText(Settings.METRIC_M);
                tvMaxSpeedUnit.setText(Settings.METRIC_MPH);
                tvAvgSpeedUnit.setText(Settings.METRIC_MPH);
                tvTripDistanceUnit.setText(Settings.METRIC_M);
                tvTripMaxSpeedUnit.setText(Settings.METRIC_MPH);
                tvTripAvgSpeedUnit.setText(Settings.METRIC_MPH);
                break;
        }
    }
    public void updateViews(){
        tvSpeed.setText(floatToNiceString(speed));
        tvDistance.setText(floatToNiceString(data.distance/1000));
        tvMaxSpeed.setText(floatToNiceString(data.maxSpeed));
        int travelTimeHours = (data.travelTime/60)/60;
        int travelTimeMinutes = (data.travelTime/60)%60;
        int travelTimeSeconds = (data.travelTime%60);
        tvTravelTime.setText(travelTimeHours + ":" + travelTimeMinutes + ":" + travelTimeSeconds);
        tvAvgSpeed.setText(floatToNiceString(((data.distance/data.travelTime)*3600)/1000));

        tvTripDistance.setText(floatToNiceString(tripdata.distance/1000));
        tvTripMaxSpeed.setText(floatToNiceString(tripdata.maxSpeed));
        int tripTravelTimeHours = (tripdata.travelTime/60)/60;
        int tripTravelTimeMinutes = (tripdata.travelTime/60)%60;
        int tripTravelTimeSeconds = (tripdata.travelTime%60);
        tvTripTravelTime.setText(tripTravelTimeHours + ":" + tripTravelTimeMinutes + ":" + tripTravelTimeSeconds);
        tvTripAvgSpeed.setText(floatToNiceString(((tripdata.distance/tripdata.travelTime)*3600)/1000));
        }

    public void onTouchSetTimers(){
        if(isMinimalView){
            isMinimalView = false;
            lMain.setBackgroundColor(Color.WHITE);
            tvSpeed.setTextColor(Color.GRAY);
            tvSpeedUnit.setTextColor(Color.GRAY);
            tcClock.setTextColor(Color.GRAY);
            btnImportData.setVisibility(View.VISIBLE);
            btnExportData.setVisibility(View.VISIBLE);
            tabBar.setVisibility(View.VISIBLE);
            timerMV.schedule(getNewTimerTask(), settings.getMinimalViewSeconds()*1000);
        }else{
            timerMV.cancel();
            timerMV = new Timer();
            timerMV.schedule(getNewTimerTask(),settings.getMinimalViewSeconds()*1000);
        }
    }

    public String floatToNiceString(Float value){
        return String.valueOf(new Float(Math.round(value*10))/10);
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(!etMinimalViewSeconds.getText().toString().isEmpty() && !etRunInBackgroundForMinutes.getText().toString().isEmpty()) {
            int seconds = Integer.parseInt(etMinimalViewSeconds.getText().toString());
            settings.setMinimalViewSeconds(seconds);
            int minutes = Integer.parseInt(etRunInBackgroundForMinutes.getText().toString());
            settings.setBackgroundMinutes(minutes);
            IO.writeSettings(settings, getFilesDir());
            onTouchSetTimers();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(position){
            case 0:
                if(!settings.getMetric().equals(Settings.METRIC_KMH)){
                    settings.setMetric(Settings.METRIC_KMH);
                    data.distance *= toKMH;
                    data.maxSpeed *= toKMH;
                    tripdata.distance *= toKMH;
                    tripdata.maxSpeed *= toKMH;
                }
                break;

            case 1:
                if(!settings.getMetric().equals(Settings.METRIC_MPH)){
                    settings.setMetric(Settings.METRIC_MPH);
                    data.distance *= toMPH;
                    data.maxSpeed *= toMPH;
                    tripdata.distance *= toMPH;
                    tripdata.maxSpeed *= toMPH;
                }
                break;
        }
        reloadText();
        updateViews();
        IO.writeSettings(settings, getFilesDir());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(locationListener);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        if(v == btnExportData){
            intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            intent.putExtra(Intent.EXTRA_TITLE, IO.dataFileName);
            intent.setType("*/*");
            startActivityForResult(intent, FILE_CHOOSER_SAVE_REQUEST_CODE);
        }else if(v == btnImportData){
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.putExtra(Intent.EXTRA_TITLE, IO.dataFileName);
            intent.setType("*/*");
            startActivityForResult(intent, FILE_CHOOSER_READ_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {

        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == FILE_CHOOSER_READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                try {
                    InputStream inputStream = getContentResolver().openInputStream(uri);
                    IO.importData(getFilesDir(), this, inputStream);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }


            }
        } else if (requestCode == FILE_CHOOSER_SAVE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                try {
                    ParcelFileDescriptor sco = this.getContentResolver().openFileDescriptor(uri, "w");
                    IO.exportData(getFilesDir(), new FileOutputStream(sco.getFileDescriptor()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void setData(Data data){
        this.data = data;
    }
}
