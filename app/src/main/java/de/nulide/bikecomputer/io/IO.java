package de.nulide.bikecomputer.io;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import de.nulide.bikecomputer.MainActivity;
import de.nulide.bikecomputer.obj.Data;
import de.nulide.bikecomputer.obj.Settings;

public class IO {

    public static final String dataFileName = "data.json";
    private static final String settingsFileName = "settings.json";

    public static void writeSettings(Settings settings, File dir) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(dir, settingsFileName);
        try {
            String json = mapper.writeValueAsString(settings);
            write(json, file);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public static Settings readSettings(File dir) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(dir, settingsFileName);
        Settings settings = new Settings();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            settings = mapper.readValue(read(br), Settings.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return settings;
    }

    public static void writeData(Data data, File dir) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(dir, dataFileName);
        try {
            String json = mapper.writeValueAsString(data);
            write(json, file);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public static Data readData(File dir) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(dir, dataFileName);
        Data data = new Data();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            data = mapper.readValue(read(br), Data.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static void write(String json, File file) {
        try {
            PrintWriter out = new PrintWriter(file);
            out.write(json);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static String read(BufferedReader br) {
        StringBuilder json = new StringBuilder();
        try {
            String line;

            while ((line = br.readLine()) != null) {
                json.append(line);
                json.append('\n');
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public static void importData(File filesDir, MainActivity mainActivity, InputStream inputStream) {
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String json = read(br);
        ObjectMapper mapper = new ObjectMapper();
        if(!json.isEmpty()) {
            try {
                Data data =  mapper.readValue(json, Data.class);
                writeData(data, mainActivity.getFilesDir());
                mainActivity.setData(data);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    public static void exportData(File filesDir, FileOutputStream fileOutputStream) {
        PrintWriter out = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            Data data = readData(filesDir);
            out = new PrintWriter(fileOutputStream);
            out.write(mapper.writeValueAsString(data));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
