package de.nulide.bikecomputer.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import de.nulide.bikecomputer.MainActivity;
import de.nulide.bikecomputer.R;

public class MainPageAdapter extends PagerAdapter {

    public MainActivity context;

    public MainPageAdapter(MainActivity context){
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        int resId = 0;
        switch (position) {
            case 0:
                resId = R.layout.layout_overall_stats;
                break;
            case 1:
                resId = R.layout.layout_trip_stats;
                break;
            case 2:
                resId = R.layout.layout_settings;
                break;
        }
        View view = collection.findViewById(resId);
        if(collection.getChildAt(position) != view) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(resId, collection, false);
            collection.addView(view, position);
            context.reloadViews();
            context.updateViews();
        }
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((TableLayout) object);
    }



    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }
}
