package de.nulide.bikecomputer.obj;

public class Settings {

    public final static String METRIC_KM = "km";
    public final static String METRIC_KMH = "km/h";
    public final static String METRIC_M = "miles";
    public final static String METRIC_MPH = "mph";

    private String metric;
    private int minimalViewSeconds;
    private int backgroundMinutes;

    public Settings(){
        metric = METRIC_KMH;
        minimalViewSeconds = 60;
        backgroundMinutes = 5;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public int getMinimalViewSeconds() {
        return minimalViewSeconds;
    }

    public void setMinimalViewSeconds(int minimalViewSeconds) {
        this.minimalViewSeconds = minimalViewSeconds;
    }

    public int getBackgroundMinutes() {
        return backgroundMinutes;
    }

    public void setBackgroundMinutes(int backgroundMinutes) {
        this.backgroundMinutes = backgroundMinutes;
    }
}
